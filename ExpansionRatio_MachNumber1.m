%{ 
To find: Expansion/area ratio
known stuff: Mach number
Author: atomw81
	   (akshay)
%}
function AR = ExpansionRatio_MachNumber1(M)
g=1.4;
gp=g+1;
gn=g-1;
AR=sqrt((1/M^2)*((2+gn*M^2)/(gp))^(gp/gn));
end