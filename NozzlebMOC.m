%{
MOC Nozzle...(Main)
Author:  atomw81
        (akshay)
%}
function NozzlebMOC
clc
tic
NChar=50;           % vmber of characteristics lines
radius=1.5;           % radius/curvature at the throat
diameter=15;         % throat diameter/width
M_exit=1.9198;        % exit Mach vmber
th_max=Prandtl_Meyer_Eqn1(M_exit);  % max isentropic turn angle

%simple region(expansion): init
M_sim=zeros(NChar,1);
X_sim=zeros(NChar,1);
Y_sim=zeros(NChar,1);
mu_sim=zeros(NChar,1);
v_sim=zeros(NChar,1);
pc_sim=zeros(NChar,1);
nc_sim=zeros(NChar,1);

M_sim(1,1)=1.0001;
X_sim(1,1)=0;
Y_sim(1,1)=diameter/2;
mu_sim(1,1)=asin(1/M_sim(1,1));
v_sim(1,1)=Prandtl_Meyer_Eqn1(M_sim(1,1));
th_sim=linspace(0,th_max/2,NChar);
pc_sim(1,1)=th_sim(1,1)-v_sim(1,1);
nc_sim(1,1)=th_sim(1,1)+v_sim(1,1);

%non-simple region(expansion): init
M_nsim=zeros(NChar,NChar);
X_nsim=zeros(NChar,NChar);
Y_nsim=zeros(NChar,NChar);
mu_nsim=zeros(NChar,NChar);
v_nsim=zeros(NChar,NChar);
th_nsim=zeros(NChar,NChar);
pc_nsim=zeros(NChar,NChar);
nc_nsim=zeros(NChar,NChar);

%straightening/cancellation region: init
M_str=zeros(NChar,1);
X_str=zeros(NChar,1);
Y_str=zeros(NChar,1);
mu_str=zeros(NChar,1);
v_str=zeros(NChar,1);
th_str=linspace(th_max/2,0,NChar);
pc_str=zeros(NChar,1);
nc_str=zeros(NChar,1);



% throat geometry and simple region
for R=2:1:NChar
    Y_sim(R,1)=Y_sim(1,1)+radius*(1-cos(th_sim(R)));
    X_sim(R,1)=X_sim(1,1)+radius*sin(th_sim(R));
    pc_sim(R,1)=pc_sim(1,1);
    v_sim(R,1)=th_sim(R)-pc_sim(R,1);
    nc_sim(R,1)=th_sim(R)+v_sim(R,1);
    M_sim(R,1)=Prandtl_Meyer_Eqn2(v_sim(R,1))  ;       %dtheta=th_sim(R,1)-th_sim(R-1,1);
    mu_sim(R,1)=asin(1/M_sim(R,1));                     %v_sim(R,1)=dtheta+v_sim(R-1,1);
end

% non-simple region
K=1;                                                        % counter
for L=1:1:NChar
    for R=K:1:NChar
        if (L==R)
            %th_nsim(R,L)=0;                                % theta=0
            nc_nsim(R,L)=nc_sim(R,1);
            v_nsim(R,L)=nc_nsim(R,L);
            M_nsim(R,L)=Prandtl_Meyer_Eqn2(v_nsim(R,L));
            mu_nsim(R,L)=asin(1/M_nsim(R,L));
            pc_nsim(R,L)=th_nsim(R,L)-v_nsim(R,L);
            
        else
            nc_nsim(R,L)=nc_sim(R,1);
            pc_nsim(R,L)=pc_nsim(R-1,L);
            th_nsim(R,L)=0.5*(nc_nsim(R,L)+pc_nsim(R,L));
            v_nsim(R,L)=0.5*(nc_nsim(R,L)-pc_nsim(R,L));
            M_nsim(R,L)=Prandtl_Meyer_Eqn2(v_nsim(R,L));
            mu_nsim(R,L)=asin(1/M_nsim(R,L));
        end
    end
    K=K+1;
end

% straightening/cancellataion region
M_str(NChar,1)=M_nsim(NChar,NChar);
mu_str(NChar,1)=asin(M_str(NChar,1));
v_str(NChar,1)=Prandtl_Meyer_Eqn1(M_str(NChar,1));
pc_str(NChar,1)=th_str(NChar)-v_str(NChar,1);
nc_str(NChar,1)=th_str(NChar)+v_str(NChar,1);

for L=1:1:NChar-1
    pc_str(L,1)=pc_nsim(NChar,L);
    v_str(L,1)=th_str(L)-pc_str(L,1);
    M_str(L,1)=Prandtl_Meyer_Eqn2(v_str(L,1));
    mu_str(L,1)=asin(1/M_str(L,1));
    nc_str(L,1)=th_str(L)+v_str(L,1);
end
% coordinates of non simple region


for R=1:1:NChar
    if(R==1)
        R_avg=tan(0.5*(th_sim(R)-mu_sim(R,1)+th_nsim(R,1)-mu_nsim(R,1)));
        X_nsim(R,1)=-Y_sim(R,1)/R_avg+X_sim(R,1);
    else
        R_avg=tan(0.5*(th_sim(R)-mu_sim(R,1)+th_nsim(R,1)-mu_nsim(R,1)));
        L_avg=tan(0.5*(th_nsim(R-1,1)+mu_nsim(R-1,1)+th_nsim(R,1)+mu_nsim(R,1)));
        X_nsim(R,1)=(Y_sim(R,1)-Y_nsim(R-1,1)-X_sim(R,1)*R_avg+X_nsim(R-1,1)*L_avg)/(L_avg-R_avg);
        Y_nsim(R,1)=(X_nsim(R,1)-X_sim(R,1))*R_avg+Y_sim(R,1);
    end
end




C=2;                                              % counter
for L=2:1:NChar
    for R=C:1:NChar
        if(L==R)
            R_avg=tan(0.5*(th_nsim(R,L-1)-mu_nsim(R,L-1)+th_nsim(R,L)-mu_nsim(R,L)));
            X_nsim(R,L)=-Y_nsim(R,L-1)/R_avg+X_nsim(R,L-1);
        else
            R_avg=tan(0.5*(th_nsim(R,L-1)-mu_nsim(R,L-1)+th_nsim(R,L)-mu_nsim(R,L)));
            L_avg=tan(0.5*(th_nsim(R-1,L)+mu_nsim(R-1,L)+th_nsim(R,L)+mu_nsim(R,L)));
            X_nsim(R,L)=(Y_nsim(R,L-1)-Y_nsim(R-1,L)-X_nsim(R,L-1)*R_avg+X_nsim(R-1,L)*L_avg)/(L_avg-R_avg);
            Y_nsim(R,L)=(X_nsim(R,L)-X_nsim(R,L-1))*R_avg+Y_nsim(R,L-1);
        end
    end
    C=C+1;
end

% straightening/cancellation region....!! CHECK !!

for L=1:1:NChar
    if(L==1)
        R_avg=tan(0.5*(th_str(1)+th_sim(1)));
        L_avg=tan(th_nsim(NChar,1)+mu_nsim(NChar,1));
        X_str(L,1)=(Y_sim(NChar,1)-Y_nsim(NChar,1)-X_sim(NChar,1)*R_avg+X_nsim(NChar,1)*L_avg)/(L_avg-R_avg);
        Y_str(L,1)=(X_str(L,1)-X_sim(NChar,1))*R_avg+Y_sim(NChar,1);
    else
        R_avg=tan(0.5*(th_str(L)+th_str(L-1)));
        L_avg=tan(th_nsim(NChar,L)+mu_nsim(NChar,L));
        X_str(L,1)=(Y_str(L-1,1)-Y_nsim(NChar,L)-X_str(L-1,1)*R_avg+X_nsim(NChar,L)*L_avg)/(L_avg-R_avg);
        Y_str(L,1)=(X_str(L,1)-X_str(L-1,1))*R_avg+Y_str(L-1,1);
    end
end
toc
X_thDiv=[X_sim(NChar,1);X_str(1,1)];
Y_thDiv=[Y_sim(NChar,1);Y_str(1,1)];
figure(2)
grid on;
hold on;
axis equal
plot(X_sim,Y_sim,'k',X_thDiv,Y_thDiv,'k',X_str,Y_str,'k')
hold on
plot(X_sim,-Y_sim,'k',X_thDiv,-Y_thDiv,'k',X_str,-Y_str,'k')
hold on;
coord1=zeros(NChar,5);
coord2=zeros(NChar,5);
coord3=zeros(NChar,3);
coord4=zeros(NChar,3);
for m=1:1:NChar
    coord1(m,1) =1;                % coordinates...for design modeler
    coord1(m,2) =m;
    coord1(m,3) =X_sim(m,1);
    coord1(m,4) =Y_sim(m,1);
    coord1(m,5)=0;
  
    coord2(m,1) =1;
    coord2(m,2) =m;
    coord2(m,3) =X_str(m,1);
    coord2(m,4) =Y_str(m,1);
    coord2(m,5)=0;
    
    coord3(m,1) =X_sim(m,1);       % coordinates...for solidworks
    coord3(m,2) =Y_sim(m,1);
    coord3(m,3)=0;
    coord4(m,1) =X_str(m,1);
    coord4(m,2) =Y_str(m,1);
    coord4(m,3)=0;
end
th_coord1=round(coord1,4);
div_coord2=round(coord2,4);
save('throat_Coords.txt', 'th_coord1', '-ascii');
type('throat_Coords.txt');
save('div_Coords.txt', 'div_coord2', '-ascii');
type('div_Coords.txt');
dlmwrite('throat_dm.txt', coord1, 'delimiter', '\t', 'precision',4);
type throat_dm.txt;
dlmwrite('div_dm.txt', coord2, 'delimiter', '\t', 'precision',4);
type div_dm.txt;
dlmwrite('solid1.txt', coord3, 'delimiter', '\t', 'precision',4);
type solid1.txt;
dlmwrite('solid2.txt', coord4, 'delimiter', '\t', 'precision',4);
type solid2.txt;
n=1;
% plotting characteristics lines... (NOTE: weird part/code, in other words, it's terrible)
for L=1:1:NChar
    for R=n:1:NChar
        if(L==1)
            x(1)=X_sim(R,1);
            x(2)=X_nsim(R,L);
            y(1)=Y_sim(R,1);
            y(2)=Y_nsim(R,L);
            plot(x,y,'c')
            hold on;
            plot(x,-y,'c')
            hold on
        else
            x(1)=X_nsim(R,L-1);
            x(2)=X_nsim(R,L);
            y(1)=Y_nsim(R,L-1);
            y(2)=Y_nsim(R,L);
            plot(x,y,'c')
            hold on;
            plot(x,-y,'c')
            hold on
        end
        if(R==NChar)
            x2(1)=X_nsim(R,L);
            x2(2)=X_str(L,1);
            y2(1)=Y_nsim(R,L);
            y2(2)=Y_str(L,1);
            plot(x2,y2,'c')
            hold on;
            plot(x2,-y2,'c')
            hold on
        else
            x2(1)=X_nsim(R+1,L);
            x2(2)=X_nsim(R,L);
            y2(1)=Y_nsim(R+1,L);
            y2(2)=Y_nsim(R,L);
            plot(x2,y2,'c')
            hold on;
            plot(x2,-y2,'c')
            hold on
        end
    end
    n=n+1;
end
end