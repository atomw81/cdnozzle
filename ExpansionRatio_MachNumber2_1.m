%{ 
To find: Mach number (M), Newton's method
known stuff: Expansion/Area ratio
Author: atomw81
	   (akshay)
%}
function M = ExpansionRatio_MachNumber2_1(AR)
g=1.4;
gp=g+1;
gn=g-1;
step=0.001;
iter=1;
error=0.1;
M=1;
A_eqn= @(M,AR1) (sqrt((1/M^2)*((2+gn*M^2)/(gp))^(gp/gn))-AR1);
k=1;
%for k=1:1:5
  while((error>1e-5))
  m=M(k);    
  M(k)=M(k)-(A_eqn(M(k),AR(k)))/((A_eqn(M(k)+step,AR(k))-A_eqn(M(k),AR(k)))/(step));
  iter=iter+1;
  error=abs(A_eqn(M(k),AR(k)));
  if(M(k)<0)
      M(k)=m/2;
  end
  end
%end
%plot(M,AR)
end