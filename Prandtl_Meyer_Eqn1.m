%{
To find: Prandtl-Meyer angle
Known stuff: Mach number (M)
Author: atomw81
	   (akshay)
%}
function v=Prandtl_Meyer_Eqn1(M)
g=1.4;
gp=g+1;
gn=g-1;
v=(sqrt(gp/gn)*atand(sqrt(gn*(M^2-1)/gp))-atand(sqrt(M^2-1)))*(pi/180);
end