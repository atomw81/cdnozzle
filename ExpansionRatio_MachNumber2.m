%{ 
To find: Mach number (M), using vpasolve();
known stuff: Expansion/Area ratio
Author: atomw81
	   (akshay)
%}
function M = ExpansionRatio_MachNumber2(AR)
g=1.4;
gp=g+1;
gn=g-1;
syms m
M=vpasolve(sqrt((1/m^2)*((2+gn*m^2)/(gp))^(gp/gn))-AR,[1,Inf]);
end