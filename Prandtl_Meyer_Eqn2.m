%{
To find: Mach number (M), using Newton's Method
Known stuff: Prandtl-Meyer angle
Author: atomw81
	   (akshay)
%}
function M=Prandtl_Meyer_Eqn2(v)
g=1.4;
gp=g+1;
gn=g-1;
%v=v*pi/180;
PM=@(m) (sqrt(gp/gn)*atan(sqrt(gn*(m^2-1)/gp))-atan(sqrt(m^2-1))-v);
M=1;
step=0.1;
iter=1;
error=1;
    while(error>0.01)
    M=M -(PM(M))/((PM(M+step)-PM(M))/step);
    error=abs(PM(M));
    iter=iter+1;
    end
end